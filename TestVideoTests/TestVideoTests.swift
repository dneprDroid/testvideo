//
//  TestVideoTests.swift
//  TestVideoTests
//
//  Created by user on 2/11/20.
//  Copyright © 2020 user. All rights reserved.
//

import XCTest
@testable import TestVideo

class TestVideoTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testComments() {
        let repliesOriginal: [CommentModel] = [
            CommentModel(userName: "C1", comment: "C1", avatarURL: "http://kkdkdkd.com/k31.png"),
            CommentModel(userName: "C2", comment: "C2", avatarURL: "http://kkdkdkd.com/k32.png"),
            CommentModel(userName: "C3", comment: "C3", avatarURL: "http://kkdkdkd.com/k33.png"),
        ]
        let original: [CommentModel] = [
            CommentModel(userName: "A", comment: "A", avatarURL: "http://kkdkdkd.com/k1.png"),
            CommentModel(userName: "B", comment: "B", avatarURL: "http://kkdkdkd.com/k2.png"),
            CommentModel(userName: "C", comment: "C", avatarURL: "http://kkdkdkd.com/k3.png", replies: repliesOriginal)
        ]
        typealias VMComment = VideoViewModel.FlatComment

        /* expected values */
        let expected: [VMComment] = [
            VMComment(userName: "A", comment: "A", avatarURL: "http://kkdkdkd.com/k1.png"),
            VMComment(userName: "B", comment: "B", avatarURL: "http://kkdkdkd.com/k2.png"),
            VMComment(userName: "C", comment: "C", avatarURL: "http://kkdkdkd.com/k3.png"),

            VMComment(userName: "C1", comment: "C1", avatarURL: "http://kkdkdkd.com/k31.png", depth: 1),
            VMComment(userName: "C2", comment: "C2", avatarURL: "http://kkdkdkd.com/k32.png", depth: 1),
            VMComment(userName: "C3", comment: "C3", avatarURL: "http://kkdkdkd.com/k33.png", depth: 1)
        ]
        /* invalid values */
        let invalid: [VMComment] = [
            VMComment(userName: "A", comment: "A!", avatarURL: "http://kkdkdkd.com/k1.png"),
            VMComment(userName: "B", comment: "B", avatarURL: "http://kkdkdkd.com/k2.png"),
            VMComment(userName: "C!", comment: "C", avatarURL: "http://kkdkdkd.com/k3---.png"),
            
            VMComment(userName: "C1", comment: "C1", avatarURL: "http://kkdkdkd.com/k31.png", depth: 0),
            VMComment(userName: "C2", comment: "C2", avatarURL: "http://kkdkdkd.com/k32.png", depth: 0),
            VMComment(userName: "C3", comment: "C3", avatarURL: "http://kkdkdkd.com/k33.png", depth: 0)
        ]

        let generated: [VMComment] = CommentsHelper.process(original)

        XCTAssertTrue(generated.count == expected.count)
        
        XCTAssertNotEqual(invalid, generated,
                          "Please check `VideoViewModel.FlatComment` Equatable protocol implementation!")
        
        for i in 0..<expected.count {
            let exp = expected[i]
            let gen = generated[i]
            XCTAssertEqual(
                exp, gen,
                "Please check `CommentsHelper.process` method" +
                " for items (index = \(i): \(exp) and \(gen)"
            )
        }
    }

}
