//
//  BaseViewController.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit


class BaseViewController : UIViewController, UIViewControllerStoryboardUtils {
    
    class func getStoryboard()->UIStoryboard {
        fatalError("\(#function) is not implemented in \(self)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }
}
