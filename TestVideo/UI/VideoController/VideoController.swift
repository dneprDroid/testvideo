//
//  VideoController.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit
import RxSwift
import RxKeyboard
import RxCocoa

class VideoController : BaseViewController {
    
    @IBOutlet fileprivate weak var conBottom: NSLayoutConstraint!
    @IBOutlet fileprivate weak var conCommentViewHeight: NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var sendBtn: UIButton!
    @IBOutlet fileprivate weak var showCommentsBtn: UIButton!
    @IBOutlet fileprivate weak var videoView: VideoPlayerView!
    @IBOutlet fileprivate weak var commentTextView: UITextView!
    @IBOutlet fileprivate weak var bottomBar: UIView!
    @IBOutlet fileprivate weak var playerPullView: PullView!
    @IBOutlet fileprivate weak var commentsView: CommentsView!

    private let dispBag = DisposeBag()

    private var statusBarHidden = false
    
    var viewModel: VideoViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurePlayerView()
        configureComments()
        configureBottomBar()
        configureTapActions()
        hideCommentsBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fetchComments()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        videoView.playLoop()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        videoView.pause()
    }
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        let isLandscape = UIDevice.current.orientation.isLandscape
        setContent(fullscreen: isLandscape)
    }
    
    override var prefersStatusBarHidden: Bool { return statusBarHidden }
    
    override class func getStoryboard()->UIStoryboard {
        return UIStoryboard(name: "Video", bundle: nil)
    }
}


//MARK: Configuration
fileprivate extension VideoController {
    
    func configurePlayerView() {
        videoView.url = viewModel.videoURL
        playerPullView.onPullListener = { [weak self] direction in
            if UIDevice.current.orientation.isLandscape {
                return
            }
            switch direction {
            case .up:
                self?.setContent(fullscreen: false)
                break
            case .down:
                self?.setContent(fullscreen: true)
                break
            default:
                break
            }
        }
    }
    
    func setContent(fullscreen: Bool) {
        if !fullscreen {
            self.showComments()
            return
        }
        self.showCommentsBtn.isHidden = false
        self.updateStatusBar(hidden: true)
        self.videoView.layoutState = .fullscreen
    }
    
    func hideCommentsBtn() {
        self.showCommentsBtn.isHidden = true
    }
    
    func showComments() {
        hideCommentsBtn()
        self.updateStatusBar(hidden: false)
        self.videoView.layoutState = .normal
    }
    
    func configureBottomBar() {
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { [weak conBottom] keyboardVisibleHeight in
                conBottom?.constant = keyboardVisibleHeight
        })
        .disposed(by: dispBag)
        
        commentTextView.textContainerInset = UIEdgeInsets.with(offset: Sizes.CommentTextInset)
        commentTextView.autoAdjustHeight(min: commentTextView.cornerRadius * 2,
                                         max: Sizes.CommentTextMaxHeight,
                                         heightConstraint: conCommentViewHeight,
                                         disposeBag: dispBag)
    }
    
    func configureComments() {
        commentsView.onScrolled = { [weak self] in
            self?.view.endEditing(true)
        }
        commentsView.commentsDatasource = viewModel
        viewModel.comments.asObservable()
        .subscribe(onNext: { [weak self] _ in
            self?.commentsView.reload()
        })
        .disposed(by: dispBag)
    }
    
    func updateStatusBar(hidden: Bool) {
        self.statusBarHidden = hidden
        setNeedsStatusBarAppearanceUpdate()
    }
}

//MARK: Taps
private extension VideoController {
    
    func configureTapActions() {
        sendBtn.rx.tap.bind { [weak self] in
            guard let text = self?.commentTextView.text,
                  !text.isEmpty else {
                return
            }
            self?.commentTextView.text = .none
            self?.viewModel.postComment(text: text)
            self?.commentsView.scrollToEnd()
        }.disposed(by: dispBag)
        
        showCommentsBtn.rx.tap.bind { [weak self] in
            self?.showComments()
        }.disposed(by: dispBag)
    }
}
