//
//  CommentsView.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit

protocol CommentsDataSourceable : class {
    var commentItems: [VideoViewModel.FlatComment] { get }
}

class CommentsView : UITableView {
    
    weak var commentsDatasource: CommentsDataSourceable?
    var onScrolled: (()->Void)?
    
    func setup() {
        self.backgroundColor = .white
        self.rowHeight = UITableView.automaticDimension
        self.allowsSelection = false
        self.separatorStyle = .none
        self.dataSource = self
        self.delegate = self
        self.registerNib(for: CommentsCell.self,
                         identifier: CommentsCell.ID)
    }
    
    func reload() {
        self.reloadData()
    }
    
    func scrollToEnd() {
        guard let count = commentsDatasource?.commentItems.count else { return }
        self.scrollToRow(at: IndexPath(row: count-1, section: 0),
                         at: .bottom, animated: true)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
}

extension CommentsView : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsDatasource?.commentItems.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let comment = commentsDatasource?.commentItems[indexPath.row] else {
            return .init()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: CommentsCell.ID,
                                                 for: indexPath) as! CommentsCell
        cell.update(with: comment)
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        onScrolled?()
    }
}
