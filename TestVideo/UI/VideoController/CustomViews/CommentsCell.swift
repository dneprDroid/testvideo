//
//  CommentsCell.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit
import Nuke



private let LeftOffsetStep: CGFloat = 25
private let MaxLeftOffset: CGFloat = 80
private let MinLeftOffset: CGFloat = 15

private let DefaultAvatarSize: CGFloat = 46

class CommentsCell : UITableViewCell {
    static let ID = "CommentsCell"

    
    @IBOutlet private weak var conAvatarSize: NSLayoutConstraint!
    @IBOutlet private weak var conLeftOffset: NSLayoutConstraint!
    
    @IBOutlet private weak var avatarView: UIImageView!
    @IBOutlet private weak var commentLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .white
        self.backgroundColor = .white
    }
    
    func update(with comment: VideoViewModel.FlatComment) {
        if comment.depth == 0 {
            conAvatarSize.constant = DefaultAvatarSize
            avatarView.cornerRadius = DefaultAvatarSize/2
        } else {
            let size = DefaultAvatarSize / 1.5
            conAvatarSize.constant = size
            avatarView.cornerRadius = size/2
        }
        if let url = comment.avatarURL {
            Nuke.loadImage(with: url, into: avatarView)
        } else {
            avatarView.image = .none
        }
        commentLabel.text = comment.comment
        if comment.userName.isEmpty {
            comment.userName = "Some user"
        } else {
            titleLabel.text = comment.userName
        }
        
        var offset = MinLeftOffset + CGFloat(comment.depth) * LeftOffsetStep
        offset = min(offset, MaxLeftOffset)
        conLeftOffset.constant = offset
        
        self.layoutIfNeeded()
    }
}
