//
//  VideoViewModel.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import RxSwift
import RxRelay

class VideoViewModel {
    
    private let dispBag = DisposeBag()
    
    var videoURL: URL
    var comments: BehaviorRelay<[VideoViewModel.FlatComment]>
    
    init(videoURL: URL) {
        self.videoURL = videoURL
        self.comments = .init(value: [])
    }
    
    func postComment(text: String) {
        comments.accept(comments.value + [FlatComment(text: text)])
    }
    
    func fetchComments() {
        API.Comments.loadComments()
            .flatMap(CommentsHelper.processToObservable)
            .observeOn(MainScheduler.instance)
            .bind(to: comments)
            .disposed(by: dispBag)
    }
}

extension VideoViewModel {
    
    class FlatComment {
        var userName: String
        var comment: String
        var avatarURL: URL?
        var depth = 0
        
        init(text: String) {
            self.userName = ""
            self.comment = text
        }
        
        init(userName: String,
             comment: String,
             avatarURL: String?,
             depth: Int = 0) {

            self.userName = userName
            self.comment = comment
            if let url = avatarURL {
                self.avatarURL = URL(string: url)
            }
            self.depth = depth
        }

        
        init(comment c: CommentModel) {
            self.userName = c.userName
            self.comment = c.comment
            if let url = c.avatarURL {
                self.avatarURL = URL(string: url)
            }
            self.depth = c.depth
        }
    }
}


extension VideoViewModel.FlatComment : Equatable {
    
    static func == (lhs: VideoViewModel.FlatComment, rhs: VideoViewModel.FlatComment) -> Bool {
        return (
            lhs.userName == rhs.userName &&
            lhs.comment == rhs.comment &&
            lhs.avatarURL == rhs.avatarURL &&
            lhs.depth == rhs.depth
        )
    }
}

extension VideoViewModel : CommentsDataSourceable {
    var commentItems: [FlatComment] { comments.value }
}

extension VideoViewModel.FlatComment : CustomStringConvertible {
    
    var description: String {
        return "FlatComment { " +
            "userName = '\(userName)', " +
            "comment = '\(comment)', " +
            "avatarURL = '\(avatarURL?.absoluteString ?? "nil")', " +
            "depth = \(depth)" +
        " }"
    }
}
