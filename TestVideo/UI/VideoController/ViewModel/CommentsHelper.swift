//
//  CommentsHelper.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import RxSwift


final class CommentsHelper {
    
    static func processToObservable(_ fetched: [CommentModel]) -> Observable<[VideoViewModel.FlatComment]> {
        return Observable.just(process(fetched))
    }
    
    static func process(_ fetched: [CommentModel]) -> [VideoViewModel.FlatComment] {
        if fetched.isEmpty {
            return []
        }
        var source = fetched
        var prepared:[VideoViewModel.FlatComment] = []
        while !source.isEmpty {
            let comment = source.removeFirst()
            prepared.append(
                VideoViewModel.FlatComment(comment: comment)
            )
            
            if let replies = comment.replies,
                !replies.isEmpty {
                
                source.insert(contentsOf: replies.map {
                    $0.depth = comment.depth + 1
                    return $0
                }, at: 0)
            }
        }
        return prepared
    }
}



