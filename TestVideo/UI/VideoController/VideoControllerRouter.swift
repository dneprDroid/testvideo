//
//  VideoControllerRouter.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit


final class VideoControllerRouter {
    
    
    static func open(nav: UINavigationController, videoURL: URL) {
        let sb = VideoController.getStoryboard()
        let vc = sb.instantiateViewController(withIdentifier: VideoController.identifier) as! VideoController
        vc.viewModel = VideoViewModel(videoURL: videoURL)
        nav.pushViewController(vc, animated: false)
    }
}
