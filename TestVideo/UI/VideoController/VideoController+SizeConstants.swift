//
//  VideoController+SizeConstants.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit

extension VideoController {
    struct Sizes {
        static let CommentTextInset: CGFloat = 10
        static let CommentTextMaxHeight: CGFloat = 100
    }
}
