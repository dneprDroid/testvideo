//
//  VideoPlayer.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit
import AVFoundation


private struct FrameState {
    var oldConstrains: [NSLayoutConstraint]
}

class VideoPlayerView : UIView {
    
    enum LayoutState {
        case normal, fullscreen
    }
    private var frameState = FrameState(oldConstrains: [])
    
    private lazy var player = AVPlayer()
    private var videoAsset: AVAsset?
    
    public override class var layerClass: AnyClass {
        get {
            return AVPlayerLayer.self
        }
    }
    
    internal var playerLayer: AVPlayerLayer {
        get {
            return self.layer as! AVPlayerLayer
        }
    }
    
    var url: URL? {
        didSet {
            if let url = url {
                onSet(videoURL: url)
            }
        }
    }
    
    var layoutState: LayoutState = .normal {
        didSet {
            if layoutState == oldValue { return }
            onSet(layoutState: layoutState)
        }
    }
    
    //MARK: public
    
    public func setup() {
        player.actionAtItemEnd = .none
        
        playerLayer.player = player
        playerLayer.isHidden = false
        playerLayer.videoGravity = .resizeAspectFill
    }
    
    public func playLoop() {
        if url == .none {
            return
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: .AVPlayerItemDidPlayToEndTime,
                                               object: player.currentItem)
        player.seek(to: .zero)
        player.play()
    }
    
    public func pause() {
        player.pause()
    }
    
    //MARK: private 

    private func onSet(videoURL: URL) {
        configurePlayerItem(.none)
        let asset = AVAsset(url: videoURL)
        configureAsset(asset)
    }
    
    private func configureAsset(_ asset: AVAsset,
                                loadableKeys: [String] = ["tracks", "playable", "duration"]) {
        
        self.videoAsset = asset
        
        self.videoAsset?.loadValuesAsynchronously(forKeys: loadableKeys, completionHandler: { () -> Void in
            guard let asset = self.videoAsset else {
                return
            }
            
            for key in loadableKeys {
                var error: NSError? = nil
                let status = asset.statusOfValue(forKey: key, error: &error)
                if status == .failed {
                    playerLog(method: "configureAsset",
                              "error: status == .failed, error: \(String(describing: error))")
                    return
                }
            }
            
            if !asset.isPlayable {
                playerLog(method: "configureAsset", "!asset.isPlayable")
                return
            }
            
            let playerItem = AVPlayerItem(asset: asset)
            self.configurePlayerItem(playerItem)
        })
    }
    
    private func configurePlayerItem(_ item: AVPlayerItem?) {
        self.player.replaceCurrentItem(with: item)
    }
    
    private func onSet(layoutState: LayoutState) {
        switch layoutState {
            case .fullscreen:
                self.showFullscreenLayout()
                break
            case .normal:
                self.showNormalLayout()
                break
        }
    }
    
    private func showFullscreenLayout() {
        guard let superview = self.superview else {
            return
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            for c in superview.constraints {
                if c.firstItem as? UIView != self &&
                    c.secondItem as? UIView  != self {
                    continue
                }
                c.isActive = false
                self.frameState.oldConstrains.append(c)
                self.removeConstraint(c)
            }
            for c in self.constraints {
                if c.firstAttribute != .height &&
                    c.firstAttribute != .width {
                    continue
                }
                c.isActive = false
                self.frameState.oldConstrains.append(c)
                self.removeConstraint(c)
            }
            let newConstrains = [
                self.topAnchor.constraint(equalTo: superview.topAnchor),
                self.bottomAnchor.constraint(equalTo: superview.bottomAnchor),
                self.leftAnchor.constraint(equalTo: superview.leftAnchor),
                self.rightAnchor.constraint(equalTo: superview.rightAnchor),
            ]
            NSLayoutConstraint.activate(newConstrains)
            self.layoutIfNeeded()
        })
    }
    
    private func showNormalLayout() {
        guard let superview = self.superview else {
            return
        }
        UIView.animate(withDuration: 0.3, animations: {
            for c in superview.constraints {
                if c.firstItem as? UIView != self &&
                    c.secondItem as? UIView  != self {
                    continue
                }
                c.isActive = false
                self.removeConstraint(c)
            }
            NSLayoutConstraint.activate(self.frameState.oldConstrains)

            self.layoutIfNeeded()
        }, completion: { _ in
            self.frameState.oldConstrains = []
        })
    }
    
    @objc private func playerItemDidReachEnd(notification: Notification) {
        if let playerItem = notification.object as? AVPlayerItem {
            playerItem.seek(to: .zero)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    deinit {
        self.player.pause()
        NotificationCenter.default.removeObserver(self)
    }
    
}


fileprivate func playerLog(method: String, _ msg: String) {
    print("[VideoPlayerView][\(method)] \(msg)")
}
