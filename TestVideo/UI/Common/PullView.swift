//
//  PullView.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit
import PureLayout

class PullView : UIView {
    typealias OnPullListener = (UISwipeGestureRecognizer.Direction)->Void
    
    private struct Sizes {
        static let lineHeight: CGFloat = 3
        static let xOffset = UIScreen.main.bounds.width/4
    }
    
    @IBInspectable
    var color: UIColor? {
        didSet {
            updateColor()
        }
    }
    
    var onPullListener: OnPullListener?
    
    private let line = UIView()
    
    func setup() {
        self.backgroundColor = .clear
        self.addSwipeListener(target: self, action: #selector(onPullUp), direction: .up)
        self.addSwipeListener(target: self, action: #selector(onPullDown), direction: .down)
        addSubview(line)
        
        line.layer.cornerRadius = Sizes.lineHeight/2
        line.autoSetDimension(.height, toSize: Sizes.lineHeight)
        line.autoPinEdge(.leading, to: .leading, of: self, withOffset: Sizes.xOffset)
        line.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: -Sizes.xOffset)
        line.autoPinEdge(.bottom, to: .bottom, of: self)
    }
    
    private func updateColor() {
        line.backgroundColor = color
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    
    @objc private func onPullUp() {
        onPullListener?(.up)
    }
    
    
    @objc private func onPullDown() {
        onPullListener?(.down)
    }
}
