//
//  MainNavigationController.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {

   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        VideoControllerRouter.open(nav: self,
                                   videoURL: Constants.TestVideoURl)
    }

}

