//
//  CommentModel.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import Foundation


class CommentModel : Codable {
    
    var userName: String
    var comment: String
    var avatarURL: String?
    var replies: [CommentModel]?
    
    var depth = 0
    
    init(userName: String,
         comment: String,
         avatarURL: String?,
         depth: Int = 0,
         replies: [CommentModel]? = []) {
        
        self.userName = userName
        self.comment = comment
        self.avatarURL = avatarURL
        self.replies = replies
        self.depth = depth
    }

    private enum CodingKeys : String, CodingKey {
        case userName = "user_name"
        case comment
        case avatarURL = "avatar"
        case replies
    }
}
