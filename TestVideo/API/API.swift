//
//  API.swift
//  TestVideo
//
//  Created by user on 2/10/20.
//  Copyright © 2020 user. All rights reserved.
//

import RxSwift


struct API {
    
    
    struct Comments {
        
        static func loadComments()->Observable<[CommentModel]> {
            // simulate loading from the server
            return Observable.create({ observer in
                do {
                    let url = Bundle.main.url(forResource: "comments", withExtension: "json")!
                    let data = try Data(contentsOf: url)
                    let comments = try JSONDecoder().decode([CommentModel].self,
                                                            from: data)
                    observer.onNext(comments)
                } catch let error {
                    observer.onError(error)
                }
                observer.onCompleted()
                return Disposables.create()
            })
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
        }
        
    }
}
