
import UIKit
import RxSwift
import RxCocoa

extension UIView {
    
    func addSwipeListener(target: Any, action: Selector, directions: [UISwipeGestureRecognizer.Direction]) {
        for direction in directions {
            addSwipeListener(target: target, action: action, direction: direction)
        }
    }
    
    func addSwipeListener(target: Any, action: Selector, direction: UISwipeGestureRecognizer.Direction) {
        self.isUserInteractionEnabled = true
        let swipe = UISwipeGestureRecognizer(target: target, action: action)
        swipe.direction = direction
        swipe.cancelsTouchesInView = false
        addGestureRecognizer(swipe)
    }
    
    static var nibName:String {
        return String(describing: self)
    }
    
    @discardableResult
    func loadNib()->UIView {
        let view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        return view
    }
    
    
    static var nib: UINib {
        let id = nibName
        return UINib(nibName: id, bundle: Bundle(for: self))
    }
    
    func loadFromNib<T: UIView>() -> T {
        let nib = type(of: self).nib
        let arr = nib.instantiate(withOwner: self, options: .none)
        return arr[0] as! T
    }
    
    func addOnTapListener(target: Any, action: Selector) {
        self.isUserInteractionEnabled = true
        let onTap = UITapGestureRecognizer(target: target, action: action)
        onTap.numberOfTapsRequired = 1
        onTap.cancelsTouchesInView = false
        addGestureRecognizer(onTap)
    }
    
    
    @IBInspectable
    var shadowColor:UIColor? {
        set {
            layer.shadowColor = newValue!.cgColor
        }
        get {
            if let color = layer.shadowColor {
               return  UIColor(cgColor: color)
            }
            return nil
        }
    }
    
    @IBInspectable
    var shadowRadius:CGFloat {
        set {
            layer.shadowRadius = newValue
        }
        get { return layer.shadowRadius }
    }
    
    @IBInspectable
    var shadowOpacity:Float {
        set {
            layer.shadowOpacity = newValue
        }
        get { return layer.shadowOpacity }
    }
    
    @IBInspectable
    var shadowOffset:CGSize {
        set {
            layer.shadowOffset = newValue
        }
        get { return layer.shadowOffset }
    }
    
    @IBInspectable
    var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get { return layer.cornerRadius }
    }
    
    @IBInspectable
    var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get { return layer.borderWidth }
    }
    
    @IBInspectable
    var borderColor:UIColor {
        set {
            layer.borderColor = newValue.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return .white
        }
    }
    
    func snapshot()->UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage()
    }
}


extension UITableView {
    
    func registerNib<T>(for cellClass: T.Type) where T: UITableViewCell {
        let identifier = String(describing: cellClass)
        registerNib(for: T.self, identifier: identifier)
    }
    
    func registerNib<T>(for cellClass: T.Type,
                        identifier: String) where T: UITableViewCell {
        register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
}

extension UIEdgeInsets {
    static func with(offset: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: offset, left: offset, bottom: offset, right: offset)
    }
}

extension UITextView {
    
    func autoAdjustHeight(min minHeight: CGFloat,
                          max maxHeight: CGFloat,
                          heightConstraint: NSLayoutConstraint,
                          disposeBag: DisposeBag) {
        self.rx.text.asObservable()
            .subscribe(onNext: { [weak self, weak heightConstraint] _ in
                guard let textView = self else { return }
                if textView.contentSize.height >= maxHeight {
                    textView.isScrollEnabled = true
                } else {
                    let height = textView.contentSize.height
                    heightConstraint?.constant = max(minHeight, height)
                    textView.layoutIfNeeded()
                }
            }).disposed(by: disposeBag)
    }
}
