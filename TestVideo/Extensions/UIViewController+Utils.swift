
import UIKit

extension UIViewController {
    static var identifier:String {
        return String.init(describing: self)
    }
}

protocol  UIViewControllerStoryboardUtils : NSObjectProtocol {
    static func fromStoryboard() -> Self
}

extension UIViewControllerStoryboardUtils where Self : BaseViewController {
    static func fromStoryboard() -> Self {
        return  getStoryboard().instantiateViewController(withIdentifier: identifier) as! Self
    }
}
